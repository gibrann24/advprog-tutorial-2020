package id.ac.ui.cs.tutorial1.service;

import id.ac.ui.cs.tutorial1.model.GuildEmployee;

import java.util.Date;
import java.util.List;

public interface GuildEmployeeService {
    public GuildEmployee addEmployee(String name, String familyName, String birthDate, String type );
    public GuildEmployee findById(String id);
    public List<GuildEmployee> findAll();
}

